import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { URLS } from "src/app/shared/constants"

@Component({
  selector: "app-homepage",
  template: "",
  styleUrls: []
})
export class HomepageComponent {
  constructor(private router: Router) {
      this.goToDashboard();
  }
  goToDashboard() {
    this.router.navigate([URLS.dashboard]);
  }
}
