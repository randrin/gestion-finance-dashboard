import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { VistoPageComponent } from "./components/visto-page/visto-page.component";
import { VistatiComponent } from "./components/vistati/vistati.component";
import { DaVistareComponent } from "./components/da-vistare/da-vistare.component";
import { GenericVistoTabPageComponent } from "./components/generic-visto-tab-page/generic-visto-tab-page.component";

const routes: Routes = [
  {
    path: "visto",
    component: VistoPageComponent,
    children: [
      {
        path: "vistati",
        component: VistatiComponent
      },
      {
        path: "da-vistare",
        component: DaVistareComponent,
        children: [
          {
            path: "tutti",
            component: GenericVistoTabPageComponent
          },
          {
            path: "bds",
            component: GenericVistoTabPageComponent
          },
          {
            path: "mandati",
            component: GenericVistoTabPageComponent
          },
          {
            path: "dirofi",
            component: GenericVistoTabPageComponent
          },
          {
            path: "giroconti",
            component: GenericVistoTabPageComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class VistoRoutingModule {}
