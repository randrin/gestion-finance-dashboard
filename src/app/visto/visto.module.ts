import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';  

import { VistoPageComponent } from "./components/visto-page/visto-page.component";
import { VistoRoutingModule } from "./visto-routing.module";
import { SharedModule } from "../shared/shared.module";
import { VistatiComponent } from "./components/vistati/vistati.component";
import { DaVistareComponent } from "./components/da-vistare/da-vistare.component";
import { GenericVistoTabPageComponent } from "./components/generic-visto-tab-page/generic-visto-tab-page.component";

@NgModule({
  declarations: [
    VistoPageComponent,
    VistatiComponent,
    DaVistareComponent,
    GenericVistoTabPageComponent
  ],
  exports: [
    VistoRoutingModule,
    VistoPageComponent,
    VistatiComponent,
    DaVistareComponent,
    GenericVistoTabPageComponent
  ],
  imports: [CommonModule, HttpClientModule, VistoRoutingModule, SharedModule],
  //entryComponents:[],
  providers: []
})
export class VistoModule {}
