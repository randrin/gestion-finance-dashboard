import { Component, OnInit } from "@angular/core";
import { Tabs } from "src/app/shared/components/models/Tab";

const getVistoTabs = {
  tabs: [
    {
      id: "daVistare",
      label: "Da vistare",
      textChildren: "43",
      url: "/visto/da-vistare"
    },
    {
      id: "vistati",
      label: "Vistati",
      textChildren: "12",
      url: "/visto/vistati"
    }
  ]
};

@Component({
  selector: "app-visto-page",
  templateUrl: "./visto-page.component.html",
  styleUrls: ["./visto-page.component.scss"]
})
export class VistoPageComponent implements OnInit {
  tabList: Tabs = [];

  constructor() {}
  ngOnInit() {
    this.mockGetVistoTabs();
  }

  mockGetVistoTabs() {
    setTimeout(() => {
      this.tabList = getVistoTabs.tabs;
    }, 1000);
  }
}
