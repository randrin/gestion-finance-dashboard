import { Component, OnInit, OnDestroy } from "@angular/core";
import { GenericRoutePageComponent } from "src/app/shared/components/generic-route-page/generic-route-page.component";
import { Subscription } from 'rxjs';
import { LoaderService, loaderKeys } from 'src/app/shared/services/loader.service';

@Component({
  selector: "app-generic-visto-tab-page",
  templateUrl: "./generic-visto-tab-page.component.html",
  styleUrls: ["./generic-visto-tab-page.component.scss"]
})
export class GenericVistoTabPageComponent extends GenericRoutePageComponent
  implements OnInit, OnDestroy {
    
  text: string;
  onDataChangedSubscription:Subscription;

  thereAreUpdates:boolean;
  diffNumber:number;

  constructor(private loader:LoaderService) {
    super(undefined, true);
  }

  ngOnInit() {
    this.text = window.location.href;
    this.listenOnDataChange();
    this.mockDifferentSourceOfDelay();
  }

  mockDifferentSourceOfDelay(){
    this.loader.showLoader(loaderKeys.global);
    console.log("mockDifferentSourceOfDelay started");
    setTimeout(() => {
      this.loader.hideLoader(loaderKeys.global);
    }, 3000);
  }

  onAutoRefresh() {
    const r = Math.floor(Math.random() * Math.floor(10));
    alert("mock autorefresh after 5 seconds");
    return r;
  }

  dataChanged(oldData, newData){
    /* console.log("old", oldData);
    console.log("new", newData); */
    return oldData !== newData
  }
  getAutoRefreshObservable(){}

  ngOnDestroy(){
    super.stopInterval();
  }

  listenOnDataChange(){
    this.onDataChangedSubscription = this.onDataChanged.subscribe(
      eventValue => {

        const { oldData, newData } = eventValue;
        if(this.dataChanged(oldData, newData)){
          this.diffNumber = newData - oldData;
          this.thereAreUpdates = true;
        }
      }
    );
  }

  showUpdates(){
    this.thereAreUpdates = false;
  }

}
