import { Component, OnInit } from "@angular/core";
import { Tabs } from "src/app/shared/components/models/Tab";
import { ComponentPairs } from 'src/app/shared/components/models/ComponentPair';
import { DummyComponent } from "src/app/shared/components/dummy/dummy.component";
import { LoaderService, loaderKeys } from 'src/app/shared/services/loader.service';

const mockedData = {
  tutti: 113,
  bds: 12,
  mandati: 11,
  dirofi: 40,
  giroconti: 60
};

const getDaVistareTabs = {
  tabs: [
    {
      id: "daVistare_Tutti",
      label: "Tutti",
      textChildren: 113,
      url: "/visto/da-vistare/tutti"
    },
    {
      id: "daVistare_BSD",
      label: "BSD",
      textChildren: 12,
      url: "/visto/da-vistare/bds"
    },
    {
      id: "daVistare_Mandati",
      label: "Mandati",
      textChildren: 11,
      url: "/visto/da-vistare/mandati"
    },
    {
      id: "daVistare_Dirofi",
      label: "Dirofi",  
      textChildren: 40,
      url: "/visto/da-vistare/dirofi"
    },
    {
      id: "daVistare_Giroconti",
      label: "Giroconti",
      textChildren: 60,
      url: "/visto/da-vistare/giroconti"
    }
  ]
};

@Component({
  selector: "app-da-vistare",
  templateUrl: "./da-vistare.component.html",
  styleUrls: ["./da-vistare.component.scss"]
})
export class DaVistareComponent implements OnInit {
  data = {};
  tabList: Tabs = [];
  componentList:ComponentPairs = [];

  constructor(private loader:LoaderService) {}
  ngOnInit() {
    this.mockFetch();
  }

  buildComponentList(){
    this.componentList = this.tabList.map(
      tab => {
        return {
          component:DummyComponent,
          componentProps:{ data:`${tab.label} - ${tab.textChildren}` },
          tabId:tab.id
        }
      }
    );
  }

  mockFetch() {
    this.loader.showLoader(loaderKeys.global);
    setTimeout(() => {
      this.data = mockedData;
      this.tabList = getDaVistareTabs.tabs;
      this.buildComponentList();
      this.loader.hideLoader(loaderKeys.global);
    }, 1000);
  }
}
