import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";

import { DashboardPageComponent } from "./components/dashboard-page/dashboard-page.component";
import { WidgetComponent } from "./components/widget/widget.component";
import { WidgetDataSetComponent } from "./components/widget-dataset/widget-dataset.component";
import { WidgetSubsetsComponent } from "./components/widget-subsets/widget-subsets.component";
import { WidgetCessioniComponent } from "./components/widget-cessioni/widget-cessioni.component";
import { SharedModule } from "../shared/shared.module";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    DashboardPageComponent,
    WidgetComponent,
    WidgetDataSetComponent,
    WidgetSubsetsComponent,
    WidgetCessioniComponent
  ],
  imports: [
    HttpClientModule,
    SharedModule,
    DashboardRoutingModule,
    CommonModule
  ],
  exports: [DashboardRoutingModule],
  entryComponents: [
    WidgetDataSetComponent,
    WidgetSubsetsComponent,
    WidgetCessioniComponent
  ],
  providers: []
})
export class DashboardModule {}
