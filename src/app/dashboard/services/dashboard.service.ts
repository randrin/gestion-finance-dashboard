import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  LoaderService,
  loaderKeys
} from "src/app/shared/services/loader.service";
import { delay, map } from "rxjs/operators";
import { forkJoin } from "rxjs";
import { HttpUtilitiesService } from "src/app/shared/services/http-utilities.service";

const URL_QUERCIA_OK = "assets/json/dashboard/initQuerciaOK.json";
const URL_VISTI_OK = "assets/json/dashboard/initVistiOK.json";
const URL_KO = "assets/json/dashboard/initKO.json";

@Injectable({
  providedIn: "root"
})
export class DashboardService {
  constructor(
    /* private http: HttpClient,
    private loader: LoaderService, */
    private httpUtilities: HttpUtilitiesService
  ) {}

  getDashboardInit(withLoader?) {
    const url1 = this.mockInit(URL_QUERCIA_OK);
    const url2 = this.mockInit(URL_VISTI_OK);
    return forkJoin([
      this.chiamataGenerica(url1, withLoader),
      this.chiamataGenerica(url2, withLoader)
    ]).pipe(
      map(input => {
        const result = { ...input[0], ...input[1] };
        return result;
      })
    );
  }

  mockInit(okUrl, koUrl = URL_KO) {
    const r = Math.floor(Math.random() * 10);
    console.log("RAND: ", r);
    if (r > 1) {
      return okUrl;
    }
    return koUrl;
  }

  chiamataGenerica(url, withLoader) {
    return this.httpUtilities.customGet({ url }, withLoader); 
  }
}
