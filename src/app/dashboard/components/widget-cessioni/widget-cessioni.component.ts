import { Component, OnInit, Input } from "@angular/core";
import { DashboardWidgetDatasets } from "src/app/shared/components/models/DashboardWidgetDataset";
import { Router } from "@angular/router";

@Component({
  selector: "app-widget-cessioni",
  templateUrl: "./widget-cessioni.component.html",
  styleUrls: ["./widget-cessioni.component.scss"]
})
export class WidgetCessioniComponent implements OnInit {
  @Input() settings = {};

  @Input() headingSettings: {};
  @Input() bodyDataSets: DashboardWidgetDatasets;
  @Input() bodySettings = {};
  @Input() footerDataSets: DashboardWidgetDatasets;
  @Input() footerSettings = {};

  constructor(private router: Router) {}
  ngOnInit() {}

  navigateTo(url) {
    this.router.navigate([url]);
  }
}
