import { Component, OnInit } from "@angular/core";
import { GenericRoutePageComponent } from "src/app/shared/components/generic-route-page/generic-route-page.component";
import { DashboardService } from "../../services/dashboard.service";

const getAutoRefreshResult = resp => resp;

@Component({
  selector: "app-dashboard-page.component",
  templateUrl: "./dashboard-page.component.html",
  styleUrls: ["./dashboard-page.component.scss"]
})
export class DashboardPageComponent extends GenericRoutePageComponent
  implements OnInit {
  data: any = {};

  constructor(private dashboard: DashboardService) {
    super({ getAutoRefreshResult });
    this.onManualRefreshClick = this.onManualRefreshClick.bind(this);
  }
  ngOnInit() {
    this.onManualRefresh.subscribe(newData => {
      this.data = newData;
    });
    this.initWithObservable();
  }

  initWithObservable() {
    return this.dashboard.getDashboardInit(true).subscribe(onSuccess => {
      const firstData = onSuccess;
      this.data = firstData;
      this.oldData = firstData;
      this.firstLoad = true;
    });
  }

  getAutoRefreshObservable(withLoader) {
    return this.dashboard.getDashboardInit(withLoader);
  }

  dataChanged(oldData, newData) {
    if (oldData && newData) {
      return JSON.stringify(oldData) !== JSON.stringify(newData);
    }
  }

  onAutoRefresh() {}

  onManualRefreshClick() {
    this.handleManualRefreshClick(true);
  }
}
