import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input
} from "@angular/core";
import { DashboardWidgetDatasets } from "src/app/shared/components/models/DashboardWidgetDataset";

const classList = {
  SINGLE_BOX: "single-box",
  MULTIPLE_BOX: "multiple-box",
  BORDER_BOTTOM_CLASS: "border-to-bottom",
  BORDER_LEFT_CLASS: "border-to-left",
  PRIMARY: "primary",
  SECONDARY: "secondary"
};

const DEFAULT_CLASS_SETTINGS = {
  borderClass: classList.BORDER_BOTTOM_CLASS,
  themeClass: classList.PRIMARY
};

@Component({
  selector: "app-widget",
  templateUrl: "./widget.component.html",
  styleUrls: ["./widget.component.scss"]
})
export class WidgetComponent implements OnInit, OnChanges {
  @Input() inputDataSets: DashboardWidgetDatasets;
  @Input() settings = {};
  @Input() withHeading: boolean;
  @Input() withFooter: boolean;

  actualSettings = {};
  boxClasses: string[] = [];

  constructor() {
    this.calculateClassesForOuterBox = this.calculateClassesForOuterBox.bind(
      this
    );
  }
  ngOnInit() {
    this.handleClasses();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.settings) {
      this.handleClasses();
    }
  }

  handleClasses() {
    this.boxClasses = [];
    this.actualSettings = { ...DEFAULT_CLASS_SETTINGS, ...this.settings };
    this.calculateClassesForOuterBox();
  }

  calculateClassesForOuterBox() {
    if (this.hasMultipleBoxes()) {
      this.swapClasses(classList.SINGLE_BOX, classList.MULTIPLE_BOX);
    } else {
      this.swapClasses(classList.MULTIPLE_BOX, classList.SINGLE_BOX);
    }

    Object.values(this.actualSettings).forEach((value: string) =>
      this.insertClassIfNotExist(value)
    );
  }

  getWidgetClass() {
    return this.boxClasses.join(" ");
  }

  swapClasses(oldClass, newClass) {
    const newClasses = this.boxClasses.filter(old => old !== oldClass);
    const indexOfNew = newClasses.indexOf(newClass);

    if (indexOfNew === -1) {
      newClasses.push(newClass);
    }

    this.boxClasses = newClasses;
  }

  insertClassIfNotExist(className: string, array = this.boxClasses) {
    if (array.indexOf(className) === -1) {
      array.push(className);
    }
  }

  hasMultipleBoxes() {
    return this.inputDataSets && this.inputDataSets.length > 1;
  }
}
