import { Component, OnInit, Input } from "@angular/core";
import { DashboardWidgetSubsets } from "src/app/shared/components/models/DashboardWidgetSubset";

@Component({
  selector: "app-widget-subsets",
  templateUrl: "./widget-subsets.component.html",
  styleUrls: ["./widget-subsets.component.scss"]
})
export class WidgetSubsetsComponent implements OnInit {
  @Input() inputSubSets: DashboardWidgetSubsets;

  constructor() {}
  ngOnInit() {}
}
