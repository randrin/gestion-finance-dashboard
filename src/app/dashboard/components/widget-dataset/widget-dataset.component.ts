import {Component,OnInit,OnChanges,SimpleChanges,Input} from "@angular/core";
import { DashboardWidgetDataset } from "src/app/shared/components/models/DashboardWidgetDataset";
import { Router } from "@angular/router";
import * as tslib from "tslib";

const classList = {
  SMALL_WIDTH: "small-width",
  MEDIUM_WIDTH: "medium-width",
  FULL_WIDTH: "full-width",
  BORDER_BOTTOM_CLASS: "border-to-bottom",
  BORDER_LEFT_CLASS: "border-to-left",
  PRIMARY: "primary",
  SECONDARY: "secondary"
};

@Component({
  selector: "app-widget-dataset",
  templateUrl: "./widget-dataset.component.html",
  styleUrls: ["./widget-dataset.component.scss"]
})
export class WidgetDataSetComponent implements OnInit, OnChanges {
  @Input() type: string;
  @Input() inputDataSet: DashboardWidgetDataset;
  @Input() classSettings: any = {};
  @Input() multiple: boolean;
  @Input() withHeading: boolean;

  boxClasses: string[] = [];
  subsetClasses: string[] = [];

  constructor(private router: Router) { }
  ngOnInit() {
    console.log(this.inputDataSet);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.inputDataSets) {
    }
    if (changes && changes.multiple) {
    }
  }

  calculateClassForSubset(dataSet: DashboardWidgetDataset) {
    if (dataSet && dataSet.subSets && dataSet.subSets.length > 0) {
      this.swapClasses(
        classList.BORDER_BOTTOM_CLASS,
        classList.BORDER_LEFT_CLASS
      );
    }
  }

  calculateClassesForOuterBox() {
    if (this.multiple) {
      this.swapClasses(
        classList.BORDER_LEFT_CLASS,
        classList.BORDER_LEFT_CLASS
      );
    }
  }

  getWidgetClass() {
    return this.boxClasses.join(" ");
  }

  swapClasses(oldClass, newClass, array = this.boxClasses) {
    const newClasses = array.filter(old => old !== oldClass);
    const indexOfNew = newClasses.indexOf(newClass);

    if (indexOfNew === -1) {
      newClasses.push(newClass);
    }

    array = newClasses;
  }

  navigate(url) {
    url && this.router.navigate([url]);
  }
}
