import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  OnDestroy
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";
import { TYPES } from "../models/Tab";
import { Subscription } from 'rxjs';

const DEFAULTS = {
  type: TYPES.PRIMARY,
  classes: [],
  selectedClass: "selected",
  url: "/",
  settings: {}
};

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.scss"]
})
export class TabComponent implements OnInit, OnChanges, OnDestroy {
  @Input() label: string;
  @Input() labelHTML: string;
  @Input() url: string = DEFAULTS.url;
  @Input() settings = DEFAULTS.settings;
  @Input() type: string = DEFAULTS.type;
  classes: string[] = DEFAULTS.classes;
  routerSubcribtion: Subscription;
  selected: boolean;

  constructor(private router: Router) {
    this.isSelected = this.isSelected.bind(this);
    this.addListenerOnRouterChange = this.addListenerOnRouterChange.bind(this);
    this.checkSelection = this.checkSelection.bind(this);
    this.getContainerClass = this.getContainerClass.bind(this);
    this.swapClasses = this.swapClasses.bind(this);
    this.updateClassesOnTypeChange = this.updateClassesOnTypeChange.bind(this);
  }

  ngOnInit() {
    this.addListenerOnRouterChange();
    this.updateClassesOnTypeChange("", this.type);
    this.checkSelection();
  }

  ngOnChanges(changes: SimpleChanges) {
    const { type } = changes;
    if (type) {
      const { previousValue, currentValue } = type;
      this.updateClassesOnTypeChange(previousValue, currentValue);
    }
  }

  ngOnDestroy(){
    this.routerSubcribtion && this.routerSubcribtion.unsubscribe();
  }

  addListenerOnRouterChange() {
    this.routerSubcribtion = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => {
        this.checkSelection();
      });
  }

  isSelected() {
    return window.location.href.includes(this.url);
  }

  checkSelection() {
    this.selected = this.isSelected();
    if (this.selected) {
      this.swapClasses("", DEFAULTS.selectedClass);
    } else {
      this.swapClasses(DEFAULTS.selectedClass, "");
    }
  }

  getContainerClass() {
    return this.classes.join(" ");
  }

  swapClasses(oldClass, newClass) {
    const newClasses = this.classes.filter(old => old !== oldClass);
    const indexOfNew = newClasses.indexOf(newClass);

    if (indexOfNew === -1) {
      newClasses.push(newClass);
    }

    this.classes = newClasses;
  }

  updateClassesOnTypeChange(oldType: string, newType: string) {
    this.swapClasses(oldType, newType);
  }
  navigate() {
    this.router.navigate([this.url]);
  }
}
