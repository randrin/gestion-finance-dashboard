import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-manual-refresh-box",
  templateUrl: "./manual-refresh-box.component.html",
  styleUrls: ["./manual-refresh-box.component.scss"]
})
export class ManualRefreshBoxComponent implements OnInit {
  @Input() onClick: Function;
  @Input() text: string;
  @Input() className: string;

  constructor() {}
  ngOnInit() {}
}
