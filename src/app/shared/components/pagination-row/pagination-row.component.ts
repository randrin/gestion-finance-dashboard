import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from "@angular/core";

const DEFAULT_SETTINGS = {
  classForFirstBtnWrapper: "col-2",
  classForMiddleBtnWrapper: "col-8",
  classForLasttnWrapper: "col-2",
  labelForFirst: "Primo",
  labelForLast: "Ultimo"
};

@Component({
  selector: "app-pagination-row",
  templateUrl: "./pagination-row.component.html",
  styleUrls: ["./pagination-row.component.scss"]
})
export class PaginationRowComponent implements OnInit, OnChanges {
  @Input() totalRows: number;
  @Input() maxRowsPerPage: number;
  @Input() maxButtonsShown: number = 5;
  @Input() settings = {};
  @Input() controlledSelectedPage: number;

  @Output() onPageChange: EventEmitter<any> = new EventEmitter();

  selectedPage: number = 1;
  auxiliarArray: number[] = [];
  maxPages: number;
  finalSettings = {};

  constructor() {}

  ngOnInit() {
    this.mainInit();
    this.combineSettings();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && (changes.totalRows || changes.maxRowsPerPage)) {
      this.mainInit();
    }
    if (changes && changes.settings) {
      this.combineSettings();
    }
  }

  mainInit() {
    this.maxPages = Math.ceil(this.totalRows / this.maxRowsPerPage);
    this.constructArray();
  }

  combineSettings() {
    this.finalSettings = { ...DEFAULT_SETTINGS, ...this.settings };
  }

  get showGoToFirst(): boolean {
    return this.selectedPage !== 1;
  }
  get showPrevious(): boolean {
    return this.auxiliarArray[0] > 1;
  }
  get showNext(): boolean {
    return this.auxiliarArray[this.maxButtonsShown - 1] < this.maxPages;
  }
  get showGoToLast(): boolean {
    return this.selectedPage !== this.maxPages;
  }

  get finalSelectedPage(): number {
    return this.controlledSelectedPage || this.selectedPage;
  }

  selectPage(pageNumber) {
    if (pageNumber !== this.selectedPage) {
      this.selectedPage = pageNumber;
      this.constructArray();
      this.onPageChange.emit(pageNumber);
    }
  }

  selectPrevious() {
    if (this.selectedPage > 1) {
      this.selectPage(this.selectedPage - 1);
    }
  }

  selectNext() {
    if (this.selectedPage < this.maxPages) {
      this.selectPage(this.selectedPage + 1);
    }
  }

  selectFirst() {
    this.selectPage(1);
  }
  selectLast() {
    this.selectPage(this.maxPages);
  }

  constructArray() {
    const newArr = [];
    const dim =
      this.maxPages < this.maxButtonsShown
        ? this.maxPages
        : this.maxButtonsShown;
    const halfDimRoundedUp = Math.ceil(dim / 2);
    let offset = 0;
    if (this.selectedPage - halfDimRoundedUp > 0) {
      offset = this.selectedPage - halfDimRoundedUp;
    }

    let lowerBound = 1 + offset;
    let upperBound = lowerBound + dim - 1;
    if (upperBound > this.maxPages) {
      upperBound = this.maxPages;
      lowerBound = upperBound - dim + 1;
    }

    for (let i = lowerBound; i <= upperBound; ++i) {
      newArr.push(i);
    }

    this.auxiliarArray = newArr;
  }
}
