import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges } from "@angular/core";
import { Options } from "../models/FadeInDropdownModels";
import { RenderType } from "../models/CustomTableModels";

@Component({
  selector: "app-fade-in-dropdown",
  templateUrl: "./fade-in-dropdown.component.html",
  styleUrls: ["fade-in-dropdown.component.scss"]
})
export class FadeInDropdown implements OnInit, OnChanges {
  @Input() options: Options = [];
  @Input() iconName:string; // o qualcosa di equivalente per renderizzare l'icona
  @Output() onClickOption: EventEmitter<any> = new EventEmitter();

  @ViewChild("optionContainer") optionContainer:ElementRef;

  isOpen: boolean = false;
  optionsToRender:Options = [];

  constructor() {
    this.closeDropdown = this.closeDropdown.bind(this);
    this.openDropdown = this.openDropdown.bind(this);
  }
  ngOnInit() {}

  ngOnChanges(){

  }

  buildOptionsAsComponents(){
    //this.optionsToRender = 
    this.options.forEach(
      op => {
        if(op.renderAs === RenderType.component){

        }
      }
    )
  }

  attachComponentToDropDown(){
    
  }

  onClick(op, event) {
    if (op && !op.disabled) {
      this.onClickOption.emit({
        option: op,
        event
      });
    }
  }

  toggleIsOpen() {
    const newValue = !this.isOpen;
    if (newValue) {
      this.openDropdown();
    } else {
      this.closeDropdown();
    }
  }

  closeDropdown() {
    this.isOpen = false;
    document.body.removeEventListener("click", this.closeDropdown);
  }

  openDropdown() {
    this.isOpen = true;
    setTimeout(() => {
      document.body.addEventListener("click", this.closeDropdown);
    }, 0);
  }

  renderAsText(option){
    return option.renderAs === RenderType.text;
  }

  renderAsIcon(option){
    return option.renderAs === RenderType.icon;
  }
  
  renderAsIconAndText(option){
    return option.renderAs === RenderType.iconAndtext;
  }

  renderTheTextSpan(option){
    return this.renderAsText(option) || this.renderAsIconAndText(option);
  }
  
  renderTheIcon(option){
    return this.renderAsIcon(option) || this.renderAsIconAndText(option);
  }

}
