import { Component, OnInit } from "@angular/core";
import { Tabs } from "../../models/Tab";

const firstLevelTabs = {
  tabs: [
    {
      id: "1",
      label: "Dashboard",
      url: "/dashboard"
    },
    {
      id: "2",
      label: "Quercia",
      url: "/quercia/in-lavorazione/da-assegnare"
    },
    {
      id: "3",
      label: "Visto",
      url: "/visto"
    }
  ]
};

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  tabList: Tabs = [];

  constructor() {}

  ngOnInit() {
    this.mockGetTabFromService();
  }

  mockGetTabFromService() {
    setTimeout(() => {
      this.tabList = firstLevelTabs.tabs;
    }, 1000);
  }
}
