import {
  Component,
  OnInit,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { LoaderService, loaderKeys } from "../../../services/loader.service";
import { Subscription } from 'rxjs';

@Component({
  selector: "app-global-loader",
  templateUrl: "./global-loader.component.html",
  styleUrls: ["./global-loader.component.scss"]
})
export class GlobalLoaderComponent implements OnInit, OnDestroy {
  loading: boolean = false;
  onShowLoader: EventEmitter<any>;
  onHideLoader: EventEmitter<any>;

  showSubsciption:Subscription;
  hideSubsciption:Subscription;

  constructor(private loader: LoaderService) {
    this.onShowLoader = loader.onShowLoader;
    this.onHideLoader = loader.onHideLoader;
    this.startListening = this.startListening.bind(this);
    this.updateLocalLoader = this.updateLocalLoader.bind(this);
  }

  ngOnInit() {
    this.startListening();
  }

  startListening() {
    this.showSubsciption = this.onShowLoader.subscribe(this.updateLocalLoader);
    this.hideSubsciption = this.onHideLoader.subscribe(this.updateLocalLoader);
  }

  updateLocalLoader(key) {
    if (key === loaderKeys.global) {
      this.loading = this.loader.getLoadingByKey(key);
    }
  }

  ngOnDestroy(){
    this.showSubsciption.unsubscribe();
    this.hideSubsciption.unsubscribe();
  }
}
