import { EventEmitter, OnDestroy } from "@angular/core";
import { Observable } from "rxjs";

const getSuccessResponseByParams = (params, successResponse) =>
  params && params.getAutoRefreshResult
    ? params.getAutoRefreshResult(successResponse)
    : successResponse;

export abstract class GenericRoutePageComponent implements OnDestroy {
  private intervalTime: number = 5000;
  protected oldData: any = {};
  protected newData: any = {};
  private intervalId: any;
  protected diffDetected: boolean;
  private manualRefreshTriggered: boolean = true;
  private fetching: boolean = false;
  protected firstLoad: boolean = false;
  private handleWithPromise: boolean = false;
  private paramsForObservable: any = {};

  protected onDataChanged: EventEmitter<any> = new EventEmitter();
  protected onManualRefresh: EventEmitter<any> = new EventEmitter();

  constructor(
    paramsForObservable?: any,
    handleWithPromise?: boolean,
    initialIntervalTime?: number
  ) {
    this.setRefreshInterval = this.setRefreshInterval.bind(this);
    this.handleManualRefreshClick = this.handleManualRefreshClick.bind(this);
    this.updateDataAfterManualRefresh = this.updateDataAfterManualRefresh.bind(
      this
    );
    this.updateDataAfterInterval = this.updateDataAfterInterval.bind(this);
    this.handleAutoRefresh = this.handleAutoRefresh.bind(this);
    this.setRefreshInterval(
      initialIntervalTime || this.intervalTime,
      paramsForObservable
    );
    this.paramsForObservable = paramsForObservable;
    this.handleWithPromise = handleWithPromise;
  }

  protected abstract async onAutoRefresh(params);
  protected abstract getAutoRefreshObservable(params): Observable<any> | any;

  protected abstract dataChanged(oldData, newData);

  protected setRefreshInterval(time, params?) {
    this.intervalTime = time;
    this.stopInterval();

    this.intervalId = setInterval(async () => {
      if (this.manualRefreshTriggered && !this.fetching && this.firstLoad) {
        if (this.handleWithPromise) {
          const newData = await this.handleAutoRefresh(params);
          this.updateDataAfterInterval(newData);
        } else {
          this.fetching = true;
          this.getAutoRefreshObservable(
            params.paramsForGetObservable
          ).subscribe(successResponse => {
            const newData = getSuccessResponseByParams(params, successResponse);
            this.updateDataAfterInterval(newData);
            this.fetching = false;
          });
        }
      }
    }, time);
  }

  protected stopInterval() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  protected async handleManualRefreshClick(params) {
    this.manualRefreshTriggered = true;
    this.diffDetected = false;
    if (this.handleWithPromise) {
      const autoRefreshResult = await this.handleAutoRefresh(params);
      this.updateDataAfterManualRefresh(autoRefreshResult);
    } else {
      this.getAutoRefreshObservable(params).subscribe(successResponse => {
        const autoRefreshResult = getSuccessResponseByParams(
          this.paramsForObservable,
          successResponse
        );
        this.updateDataAfterManualRefresh(autoRefreshResult);
      });
    }
  }

  updateDataAfterManualRefresh(autoRefreshResult) {
    this.oldData = autoRefreshResult;
    this.newData = autoRefreshResult;
    this.onManualRefresh.emit(this.newData);
  }

  updateDataAfterInterval(newData) {
    const { oldData } = this;
    this.newData = newData;
    if (this.dataChanged(oldData, newData)) {
      this.onDataChanged.emit({
        oldData,
        newData
      });
      this.diffDetected = true;
      this.manualRefreshTriggered = false;
      this.oldData = this.newData;
    }
  }

  private async handleAutoRefresh(params?) {
    this.fetching = true;
    const autoRefreshResult = await this.onAutoRefresh(params);
    this.fetching = false;

    return autoRefreshResult;
  }

  ngOnDestroy() {
    this.stopInterval();
  }
}
