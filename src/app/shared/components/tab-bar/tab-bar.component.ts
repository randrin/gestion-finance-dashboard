import { Component, OnInit, Input, ElementRef, ViewChild } from "@angular/core";
import { Tabs } from "../models/Tab";
import { ComponentPairs } from "../models/ComponentPair";
import { PopoverContentComponent } from "../popover-content/popover-content.component";
import {ComponentLoader,ComponentLoaderFactory} from "ngx-bootstrap/component-loader";

@Component({
  selector: "app-tab-bar",
  templateUrl: "./tab-bar.component.html",
  styleUrls: ["./tab-bar.component.scss"]
})
export class TabBarComponent implements OnInit {
  @Input() tabList: Tabs = [];
  @Input() type: string = "primary";
  @Input() withPopover: boolean;
  @Input() popoverSettings: any = {};

  @Input() componentList: ComponentPairs = [];

  @ViewChild("thisTabBar") thisTabBar: ElementRef;

  constructor(private clf: ComponentLoaderFactory) {
    this.addLoader = this.addLoader.bind(this);
    this.getSpanContainerByTabId = this.getSpanContainerByTabId.bind(this);
    this.attachRefToSPan = this.attachRefToSPan.bind(this);
  }

  querciaLoader: ComponentLoader<PopoverContentComponent>[] = [];

  ngOnInit() {
  }

  addLoader() {
    const loader = this.clf.createLoader<PopoverContentComponent>(
      null,
      null,
      null
    );
    this.querciaLoader.push(loader);
    return loader;
  }

  getSpanContainerByTabId(tabId: string) {
    const selector = `[tab-id="${tabId}"]`;
    return this.thisTabBar.nativeElement.querySelector(selector);
  }

  getComponentPairByTabId(tabId: string) {
    return this.componentList.find(el => el.tabId === tabId);
  }

  attachRefToSPan(tabId: string) {
    const loader = this.addLoader();
    setTimeout(() => {
      const span = this.getSpanContainerByTabId(tabId);
      if (span) {
        const componentPair = this.getComponentPairByTabId(tabId);
        if (componentPair) {
          const spanAsElementRef = new ElementRef(span);
          const { component, componentProps } = componentPair;

          const ref = loader
            .attach(PopoverContentComponent)
            .to(spanAsElementRef)
            .show({
              content: component,
              initialState: componentProps
            });
        }
      }
    }, 0);
  }
}
