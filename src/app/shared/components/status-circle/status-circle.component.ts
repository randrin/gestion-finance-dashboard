import { Component, OnInit, Input } from "@angular/core";

const mapStatusToDefaults = {
  red: "Stato rosso",
  orange: "Stao arancione",
  green: "Stato verde",
  yellow:"stato giallo",
  orangeLineThrough: "Stato arancione barrato",
  greenLineThrough: "Stato verde barrato"
};

@Component({
  selector: "app-status-circle",
  templateUrl: "./status-circle.component.html",
  styleUrls: ["./status-circle.component.scss"]
})
export class StatusCircleComponent implements OnInit {
  @Input() status: string;
  @Input() popoverMessage: string;
  defaultPopoverMessage: string;

  constructor() {}

  ngOnInit() {
    this.defaultPopoverMessage = mapStatusToDefaults[this.status];
  }

  getClass() {
    return this.status;
  }

  getMessage() {
    return this.popoverMessage || this.defaultPopoverMessage;
  }
}
