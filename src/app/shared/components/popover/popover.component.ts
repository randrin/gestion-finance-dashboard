import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";

const DEFAULT_SETTINGS = {
  trianglePosition: "top",
  horizontalAlignment: "center",
  verticalAlignment:"center",
  displayStyle:"inlineBlock",
  fixedHeight:true
};

const mapSettingsToClassNames = {
  top: "triangle-top",
  bottom: "triangle-bottom",
  right: "triangle-right",
  left: "triangle-left",
  horizontalAlignment: {
    left: "horizontal-align-left",
    center: "horizontal-align-center",
    right: "horizontal-align-right"
  },
  verticalAlignment: {
    top: "vertical-align-top",
    center: "vertical-align-center",
    bottom: "vertical-align-bottom"
  }
};

const mapPositionToAlignmentObj = {
  top: "horizontalAlignment",
  bottom: "horizontalAlignment",
  right: "verticalAlignment",
  left: "verticalAlignment"
};

const mapDisplayStyleToClass = {
  inline:"inline-display",
  inlineBlock:"inline-block-display",
  block:"block-display"
}

const getPositionClassForContainer = (position = "") => `container-${position}`;

@Component({
  selector: "app-popover",
  templateUrl: "./popover.component.html",
  styleUrls: ["./popover.component.scss"]
})
export class PopoverComponent implements OnInit {
  
  @Input() inputSettings: {};
  @Input() calculatePositions:Function;
  
  show: boolean = false;
  settings: any = {};
  containerClass: string = "";
  mainClass: string = "";
  alignmentClass: string = "";
  displayClassForMainContainer:string = "";
  firstShow:boolean = false;

  @Output() onShow:EventEmitter<any> = new EventEmitter();
  @Output() onHide:EventEmitter<any> = new EventEmitter();
  @Output() onFirstShow:EventEmitter<any> = new EventEmitter();

  @ViewChild("divRef") divRef:ElementRef;

  constructor() {}

  ngOnInit() {
    this.constructSettings();
    this.calculateClassesBySettings();
  }

  constructSettings() {
    this.settings = { ...DEFAULT_SETTINGS, ...this.inputSettings };
  }

  calculateClassesBySettings() {
    const { trianglePosition, displayStyle, fixedHeight } = this.settings;
    const mainClass = mapSettingsToClassNames[trianglePosition];
    if (mainClass) {
      this.mainClass = mainClass;
      this.containerClass = getPositionClassForContainer(trianglePosition);
      const alignmentObjKey = mapPositionToAlignmentObj[trianglePosition];
      if (alignmentObjKey) {
        const alignmentProp = this.settings[alignmentObjKey];
        const alignmentObj = mapSettingsToClassNames[alignmentObjKey];
        if (alignmentProp && alignmentObj) {
          this.alignmentClass = alignmentObj[alignmentProp];
        }
      }

      const displayClass = mapDisplayStyleToClass[displayStyle];
      if(displayClass){
        this.displayClassForMainContainer = displayClass;
      }

      if(fixedHeight){
        this.containerClass += " fixed-heigth";
      }

      this.containerClass += ` ${this.alignmentClass}`;
    }
  }

  getClassForTriangle() {
    if (this.mainClass && this.alignmentClass) {
      return `${this.mainClass} ${this.alignmentClass}`;
    }

    return "";
  }

  onMouseEnter() {
    this.show = true;
    if(!this.firstShow){
      this.firstShow = true;
      this.onFirstShow.emit();
    }
    this.onShow.emit();

    if(!this.settings.fixedHeight && this.calculatePositions){
      this.calculatePositions(this.divRef);
    }
  }
  
  onMouseLeave() {
    this.show = false;
    this.onHide.emit();
  }
}
