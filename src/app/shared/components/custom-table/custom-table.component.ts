import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import {
  mapActionToClass,
  mapIconsToToggle
} from "./cutom-table-utils/custom-table-utils";

import {
  HeadConfigs,
  BodyConfigs,
  GlobalSettings,
  EmptyData,
  BodyRow
} from "../models/CustomTableModels";

const DEFAULT_SETTINGS: GlobalSettings = {
  globalSearchList: [],
  withAdvancedSearch: true,
  onAdvancedSearch: null,
  withDownloadCsv: true,
  showCurrentTotale: true,
  paginationValues: ["5", "6", "9", "20"],
  actions: [
    {
      id: "1",
      name: "Azione 1",
      visible: true,
      onClick: (event, action) => console.log(event, action)
    },
    {
      id: "2",
      name: "Azione 2",
      visible: true,
      onClick: (event, action) =>
        console.log("Click Azione 2:\n", event, action)
    }
  ],
  withRowSelection: true,
  onToggleRowSelection: null,
  onRowClick: null,
  onCellClick: null
};

@Component({
  selector: "app-custom-table",
  templateUrl: "./custom-table.component.html",
  styleUrls: ["./custom-table.component.scss"]
})
export class CustomTableComponent implements OnInit, OnChanges {
  @Input() inputHeadConfigs: HeadConfigs = [];
  @Input() inputRowConfigs: BodyConfigs = {};
  @Input() inputSettings: GlobalSettings | EmptyData = {};

  loading: boolean = false;
  closeModal: boolean = false;
  numberCessioni: number = 0;
  mapCessioni: Map<number, string> = new Map();

  headConfigs: HeadConfigs;
  rowConfigs: BodyConfigs = {};
  settings: GlobalSettings;

  selectedPage: number = 1;
  rowsForPage: number;
  maxNumberOfPages: number;
  showAdvancedSearchForm: boolean;
  searchString: string = "";
  searchResultList: BodyRow[] = [];

  constructor() {
    this.initWithoutSlice = this.initWithoutSlice.bind(this);
    this.init = this.init.bind(this);
    this.hideColumns = this.hideColumns.bind(this);
    this.sortAction = this.sortAction.bind(this);
    this.onIconClick = this.onIconClick.bind(this);
    this.toggleIcon = this.toggleIcon.bind(this);
    this.downloadCsv = this.downloadCsv.bind(this);
    this.paginate = this.paginate.bind(this);
    this.changeMaxRowsToShow = this.changeMaxRowsToShow.bind(this);
    this.isHeadEmpty = this.isHeadEmpty.bind(this);
    this.isBodyEmpty = this.isBodyEmpty.bind(this);
    this.isAllEmpty = this.isAllEmpty.bind(this);
  }

  ngOnInit() {
    this.init();
   
  
  }

  init(hideColumnsSelector?) {
    this.initWithoutSlice(hideColumnsSelector);
    this.changeByValue(this.rowsForPage, true);
  }

  initWithoutSlice(hideColumnsSelector?) {
    this.hideColumns(hideColumnsSelector);
    if (!hideColumnsSelector) {
      this.combineSettings();
      this.selectFirstRowForPage();
    }
  }

  selectFirstRowForPage() {
    if (this.settings.paginationValues && this.settings.paginationValues[0]) {
      this.rowsForPage = Number(this.settings.paginationValues[0]);
    }
  }

  combineSettings() {
    this.settings = { ...DEFAULT_SETTINGS, ...this.inputSettings };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes &&
      (changes.inputHeadConfigs ||
        changes.inputRowConfigs ||
        changes.inputSettings)
    ) {
      this.init();
    }
  }

  hideColumns(selector?: string) {
    let newHeadConfigs = this.headConfigs;
    if (
      (selector === "head" || !selector) &&
      this.inputHeadConfigs &&
      this.inputHeadConfigs.filter
    ) {
      newHeadConfigs = this.inputHeadConfigs.filter(el => el.visible !== false); //undefined is ok
      this.headConfigs = newHeadConfigs;
    }

    if (
      (selector === "body" || !selector) &&
      this.inputRowConfigs &&
      this.inputRowConfigs.list &&
      this.inputRowConfigs.list.map
    ) {
      const newRowList = this.totalRowList.map(el => {
        const newCellList = el.cellList.filter(
          cell =>
            newHeadConfigs.findIndex(
              newConf => cell.columnId === newConf.id
            ) !== -1
        );
        return { ...el, cellList: newCellList };
      });

      this.rowConfigs = {
        ...this.inputRowConfigs,
        list: this.getOrderedRowList(newRowList)
      };
    }
  }

  getOrderedRowList(filteredRowList) {
    return filteredRowList.map(row => {
      const newCellList = this.headConfigs.map(head => {
        const { id, visible } = head;
        if (visible) {
          const { cellList } = row;
          const matchingCell = cellList.find(cell => cell.columnId === id);
          if (matchingCell) {
            return matchingCell;
          } else {
            return {};
          }
        }
        return {};
      });
      return { ...row, cellList: newCellList };
    });
  }

  onIconClick(iconClicked: string, columnConfigs) {
    console.log(iconClicked, columnConfigs);
    if (iconClicked === "sort-asc" || iconClicked === "sort-des") {
      this.sortAction(columnConfigs.id, iconClicked);
    }
  }

  getClassForIcon(actionName) {
    return mapActionToClass[actionName] || "";
  }

  getReturnValueForSort(iconClicked, cell1, cell2) {
    const val1 = cell1.textValue.toString();
    const val2 = cell2.textValue.toString();
    let compareFn = (v1, v2) => v1 < v2;

    if (iconClicked === "sort-des") {
      compareFn = (v1, v2) => v2 < v1;
    }

    if (compareFn(val1, val2)) {
      return -1;
    }
    return 1;
  }

  sortAction(columnId, iconClicked) {
    this.hideColumns("body");
    const oldRows = [].concat(this.rowConfigs.list);
    const newRows = oldRows.sort((row1, row2) => {
      const { cellList: list1 } = row1;
      const { cellList: list2 } = row2;
      const cell1 = list1.find(l1 => l1.columnId === columnId);
      const cell2 = list2.find(l2 => l2.columnId === columnId);

      if (cell1 && cell2) {
        return this.getReturnValueForSort(iconClicked, cell1, cell2);
      }
      return 0;
    });
    this.rowConfigs = { ...this.rowConfigs, list: newRows };
    this.toggleIcon(columnId, iconClicked);
    this.spliceRows(this.rowsForPage, true);
  }

  toggleIcon(columnId, iconClicked) {
    const newValue = mapIconsToToggle[iconClicked];
    if (newValue) {
      const columnIndex = this.headConfigs.findIndex(el => el.id === columnId);
      if (columnIndex !== -1) {
        const oldIconList = this.headConfigs[columnIndex].iconList;
        const oldActionIndex = oldIconList.findIndex(el => el === iconClicked);
        if (oldActionIndex !== -1) {
          const newIconList = oldIconList
            .slice(0, oldActionIndex)
            .concat(newValue)
            .concat(oldIconList.slice(oldActionIndex + 1));
          const headCopy = this.headConfigs;
          headCopy[columnIndex] = {
            ...this.headConfigs[columnIndex],
            iconList: newIconList
          };
          this.headConfigs = headCopy;
        }
      }
    }
  }

  downloadCsv() {
    const createRow = array =>
      array.reduce(
        (acc, current, index) =>
          acc + current.textValue + (array.length === index + 1 ? "" : ";"),
        ""
      ) + "\n";

    const headerRow = createRow(this.inputHeadConfigs);
    let csvText = headerRow;

    this.inputRowConfigs.list.forEach(row => {
      const { cellList } = row;
      const thisRow = createRow(cellList);
      csvText += thisRow;
    });

    let blob = new Blob([csvText]);
    let blobURL = window.URL.createObjectURL(blob);

    const aTag = document.createElement("a");
    aTag.href = blobURL;
    aTag.download = "data.csv";

    document.body.appendChild(aTag);
    aTag.click();
    setTimeout(() => {
      aTag.remove();
    }, 0);
  }

  paginate(value) {
    if (value !== this.selectedPage) {
      this.selectedPage = value;
      this.spliceRows(Number(this.rowsForPage));
      this.hideColumns("head"); //reset icons' state
    }
  }

  get rowsTotal(): number {
    return this.totalRowList.length;
  }

  spliceRows(num: number, useCurrentRows: boolean = false) {
    if (
      this.inputRowConfigs &&
      this.inputRowConfigs.list &&
      this.inputRowConfigs.list.splice
    ) {
      const multiplier = this.selectedPage - 1;
      const startingIndex = multiplier * num;
      if (!useCurrentRows) {
        this.hideColumns("body");
      }
      const copyArr = [].concat(this.rowConfigs.list);
      const newList = copyArr.splice(startingIndex, num);
      this.rowConfigs = { ...this.rowConfigs, list: newList };
    }
  }

  changeByValue(value: number, useCurrentRows: boolean = false) {
    this.spliceRows(value, useCurrentRows);
    this.rowsForPage = value;
  }

  changeMaxRowsToShow(event) {
    const val = Number(event.target.value);
    this.changeByValue(val);
  }

  isHeadEmpty() {
    return !(this.inputHeadConfigs && this.inputHeadConfigs.length > 0);
  }
  isBodyEmpty() {
    return !(
      this.inputRowConfigs &&
      this.inputRowConfigs.list &&
      this.inputRowConfigs.list.length > 0
    );
  }

  isAllEmpty() {
    return this.isHeadEmpty() && this.isBodyEmpty();
  }

  onToggleCheckboxAction(row, event) {
    console.log('Selected queria: ', row);
    this.closeModal = true;
    if (event.target.checked) {
      this.numberCessioni += 1;
      this.mapCessioni.set(row.id, row);
      console.log('Selected this.mapCessioni: ', this.mapCessioni);
    } else {
      this.numberCessioni -= 1;
      this.mapCessioni.delete(row.id);
      console.log('Selected this.mapCessioni: ', this.mapCessioni);
    }
    if (this.mapCessioni.size === 0) {
      this.closeModal = false;
    }
    if (this.settings.onToggleRowSelection) {
      console.log('Selected queria inside: ', event.target.checked);
      const value = event.target.checked;
      this.settings.onToggleRowSelection(value, row, event);
    }
  }

  toggleShowAdvancedSearchForm() {
    this.showAdvancedSearchForm = !this.showAdvancedSearchForm;
  }

  onRowClick(row, event) {
    if (this.settings && this.settings.onRowClick) {
      this.settings.onRowClick(row, event);
    }
  }
  onCellClick(rowClicked, cellClicked, event) {
    if (this.settings && this.settings.onCellClick) {
      this.settings.onCellClick(rowClicked, cellClicked, event);
    }
  }
  onActionChange(event) {
    const { option: action, event: originalClickEvent } = event;

    if (action && action.onClick) {
      action.onClick(originalClickEvent, action);
    }
  }

  onGlobalSearch(event) {
    const eventValue = event.target.value;

    if (eventValue !== this.searchString) {
      this.searchString = eventValue;
      this.loading = true;
      setTimeout(() => {
        this.performGlobalSearch(eventValue);
      }, 0);
    }
  }

  performGlobalSearch(searchValue) {
    if (searchValue === "") {
      this.init("body");
      this.stopLoadingAfter100Ms();
      return;
    }

    this.hideColumns("body");

    const cellsToSearch = this.settings.globalSearchList;

    const newList: BodyRow[] = this.inputRowConfigs.list.filter(row => {
      const { cellList } = row;
      if (cellList) {
        return cellList.some(cell => {
          const { columnId } = cell;
          const searchCell = cellsToSearch.find(c => c.columnId === columnId);
          if (searchCell) {
            if (searchCell.searchFunction) {
              return searchCell.searchFunction(cell, searchValue);
            }

            return (
              cell.textValue &&
              cell.textValue
                .toLocaleLowerCase()
                .includes(searchValue.toLowerCase())
            );
          }
        });
      }
    });

    this.searchResultList = newList;
    this.rowConfigs = { ...this.rowConfigs, list: newList };

    if (newList.length < this.rowsForPage * this.selectedPage) {
      this.selectedPage = 1;
    }

    this.spliceRows(this.rowsForPage, true);
    this.stopLoadingAfter100Ms();
  }

  stopLoadingAfter100Ms() {
    setTimeout(() => {
      this.loading = false;
    }, 100);
  }

  get totalRowList() {
    if (this.inputRowConfigs && this.inputRowConfigs.list) {
      return this.searchString.length > 0 && !this.loading
        ? this.searchResultList
        : this.inputRowConfigs.list;
    }

    return [];
  }

  handleAdvancedSearchSubmit(event) {
    const { onAdvancedSearch } = this.settings;
    if (onAdvancedSearch) {
      onAdvancedSearch(event);
    }
  }
}
