export const SORT_ASC_ACTION = "sort-asc";
export const SORT_DES_ACTION = "sort-des";
export const SEARCH_ACTION = "search";

export const SUPPORTED_ICON_LIST = [
  SORT_ASC_ACTION,
  SORT_DES_ACTION,
  SEARCH_ACTION
];
export const mapIconsToToggle = {
  [SORT_ASC_ACTION]: SORT_DES_ACTION,
  [SORT_DES_ACTION]: SORT_ASC_ACTION
};

export const mapActionToClass = {
  [SORT_ASC_ACTION]: "fa fa-arrow-down",
  [SORT_DES_ACTION]: "fa fa-arrow-up",
  [SEARCH_ACTION]: "fa fa-search"
};


const getDownloadCsvAsAction = (actions, onClick, fallback) => {
  
}