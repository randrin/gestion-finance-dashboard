export class DashboardWidgetSubset{
    count:number;
    status:string;
    popoverMessage?:string
}

export declare type DashboardWidgetSubsets = DashboardWidgetSubset[];