export const TYPES = {
    PRIMARY: "primary",
    SECONDARY: "secondary",
    TERTIARY: "tertiary"
  };

export class Tab{
    id:string;
    label:string;
    textChildren?:string | number;
    url:string;
    type?:string = TYPES.PRIMARY;
}

export declare type Tabs = Tab[];