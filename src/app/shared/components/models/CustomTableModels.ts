export enum RenderType{
    text = "text",
    status = "status",
    icon = "icon",
    iconAndtext = "iconAndtext",
    component = "component"
}

export enum FormatType{
    rawText = "rawText",
    date = "date",
    currency = "currency"
}

export interface HeadConfig {
  id: string;
  visible: boolean;
  textValue: string;
  iconList: string[];
  renderType:RenderType;
  formatType:FormatType,
  formatInfo:string,
  ngClassValue:string
}

export interface GlobalSearchSetting{
    columnId:string,
    searchFunction?:Function
}

export declare type HeadConfigs = HeadConfig[];


export interface BodyCell {
  textValue: string;
  columnId: string;
}

export interface BodyRow {
  id: string;
  cellList: BodyCell[]
}

export interface BodyConfigs {
    list?:BodyRow[]
}

export class TableAction {
    id:string;
    name?:string;
    iconName?:string;
    visible:boolean;
    onClick:Function;
    renderAs?:RenderType = RenderType.iconAndtext;
    componentProps?:any;
}

export interface GlobalSettings {
  globalSearchList: GlobalSearchSetting[];
  withAdvancedSearch: boolean;
  withDownloadCsv: boolean;
  showCurrentTotale: boolean;
  paginationValues: number[] | string[];
  actions: TableAction[];
  withRowSelection: boolean;
  onToggleRowSelection?: Function;
  onRowClick?:Function;
  onCellClick?:Function;
  onAdvancedSearch?:Function
}
//export interface HeadConfigs{}

export interface EmptyData{}