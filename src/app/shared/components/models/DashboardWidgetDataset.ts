import { DashboardWidgetSubset } from "./DashboardWidgetSubset";

export class DashboardWidgetDataset {
  linkLabel: string;
  linkUrl: string;
  title?: string;
  total: number;
  subSets?: DashboardWidgetSubset[];
  colClass?:string
}

export declare type DashboardWidgetDatasets = DashboardWidgetDataset[];
