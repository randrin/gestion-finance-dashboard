export class ComponentPair{
    component:any;
    componentProps:any;
    tabId:string;
}

export declare type ComponentPairs = ComponentPair[];