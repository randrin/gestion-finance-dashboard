import { RenderType } from "./CustomTableModels";

export class Option{
    id:string;
    name?:string;
    iconName?:string;
    visible:boolean;
    onClick:Function;
    renderAs?:RenderType = RenderType.iconAndtext;
    componentProps?:any;
}

export declare type Options = Option[];