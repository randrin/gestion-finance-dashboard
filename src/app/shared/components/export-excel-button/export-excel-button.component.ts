import { Component, Input } from "@angular/core";

const DEFAULTS = {
    ngClassValue:"custom-btn as-option with-padding",
    text:"Esporta excel"
}

@Component({
  selector: "app-export-excel-button",
  templateUrl: "export-excel-button.component.html",
  styleUrls: ["export-excel-button.component.scss"]
})
export class ExportExcelButton {
  @Input() text: string = DEFAULTS.text;
  @Input() ngClassValue: string = DEFAULTS.ngClassValue;
  @Input() onClick: Function;
  @Input() fallback: Function;

  constructor() {}

  handleButtonClick(event) {
    if (this.onClick) {
      this.onClick(event);
    } else if (this.fallback) {
      this.fallback(event);
    }
  }
}
