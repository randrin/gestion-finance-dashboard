import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { DummyComponent } from "./components/dummy/dummy.component";
import { PopoverContentComponent } from "./components/popover-content/popover-content.component";
import { PageWrapperComponent } from "./components/layout/page-wrapper/page-wrapper.component";
import { HeaderComponent } from "./components/layout/header/header.component";
import { GlobalLoaderComponent } from "./components/layout/global-loader/global-loader.component";
import { CustomTableComponent } from "./components/custom-table/custom-table.component";
import { PopoverComponent } from "./components/popover/popover.component";
import { TabComponent } from "./components/tab/tab.component";
import { TabBarComponent } from "./components/tab-bar/tab-bar.component";
import { StatusCircleComponent } from "./components/status-circle/status-circle.component";
import { ManualRefreshBoxComponent } from "./components/manual-refresh-box/manual-refresh-box.component";
import { ComponentLoaderFactory } from "ngx-bootstrap/component-loader";
import { PositioningService } from "ngx-bootstrap/positioning";
import { FadeInDropdown } from './components/fade-in-dropdown/fade-in-dropdown.component';
import { PaginationRowComponent } from './components/pagination-row/pagination-row.component';
import { ExportExcelButton } from './components/export-excel-button/export-excel-button.component';
import { HeaderTopComponent } from './components/layout/header-top/header-top.component';



@NgModule({
  declarations: [
    HeaderComponent,
    CustomTableComponent,
    PopoverComponent,
    TabComponent,
    TabBarComponent,
    DummyComponent,
    PopoverContentComponent,
    PageWrapperComponent,
    GlobalLoaderComponent,
    StatusCircleComponent,
    ManualRefreshBoxComponent,
    FadeInDropdown,
    PaginationRowComponent,
    ExportExcelButton,
    HeaderTopComponent,
  ],
  exports: [
    HeaderComponent,
    CustomTableComponent,
    PopoverComponent,
    TabComponent,
    TabBarComponent,
    DummyComponent,
    PopoverContentComponent,
    PageWrapperComponent,
    GlobalLoaderComponent,
    StatusCircleComponent,
    ManualRefreshBoxComponent,
    FadeInDropdown,
    PaginationRowComponent,
    ExportExcelButton
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  entryComponents: [DummyComponent, PopoverContentComponent],
  providers: [ComponentLoaderFactory, PositioningService]
})
export class SharedModule {}
