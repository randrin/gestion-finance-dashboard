export const URLS = {
    dashboard:"/dashboard"
}

export const genericDataChangeFn = (oldData, newData) => {
    if(oldData && newData){
        const str1 = JSON.stringify(oldData);
        const str2 = JSON.stringify(newData);
        if(str1 && str2){
            return str1 !== str2;
        }
    }
}