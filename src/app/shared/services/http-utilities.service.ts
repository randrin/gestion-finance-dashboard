import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoaderService, loaderKeys } from "./loader.service";
import { map, delay } from "rxjs/operators";
import { Observable } from "rxjs";

const DEFAULT_COMMON_HEADERS = {
  "Content-Type": "application/json"
};

const combineHeadersAndGetOptions = inputOptions => {
  const inputHeaders = (inputOptions && inputOptions.headers) || {};
  return {
    ...inputOptions,
    headers: { ...DEFAULT_COMMON_HEADERS, ...inputHeaders }
  };
};

@Injectable({
  providedIn: "root"
})
export class HttpUtilitiesService {
  constructor(private http: HttpClient, private loader: LoaderService) {}

  customGet(standardGetParams, withGlobalLoader):Observable<any> {
    const { url, options } = standardGetParams;
    const actualOptions = combineHeadersAndGetOptions(options);
    return this.addDelayAndOrLoader(
      this.http.get(url, actualOptions),
      withGlobalLoader
    );
  }

  customPost(postParams, withGlobalLoader) {
    const { url, body, options } = postParams;
    const actualOptions = combineHeadersAndGetOptions(options);
    return this.addDelayAndOrLoader(
      this.http.post(url, body, actualOptions),
      withGlobalLoader
    );
  }

  addDelayAndOrLoader(
    observableObj: Observable<any>,
    withGlobalLoader,
    withDelay = true,
    withGenericErrorHandler = true
  ) {
    let resultObservable = observableObj;
    withGlobalLoader && this.loader.showLoader(loaderKeys.global);

    if (withDelay) {
      resultObservable = resultObservable.pipe(delay(2000));
    }
    if (withGlobalLoader) {
      resultObservable = resultObservable.pipe(
        map(success => {
          withGlobalLoader && this.loader.hideLoader(loaderKeys.global);
          return success;
        }),
        map(error => {
          withGlobalLoader && this.loader.hideLoader(loaderKeys.global);
          return error;
        })
      );
    }

    if (withGenericErrorHandler) {
      resultObservable = resultObservable.pipe(
        onSuccess => onSuccess,
        map(this.genericErrorHandler)
      );
    }

    return resultObservable;
  }

  genericErrorHandler(error) {
    //logic here
   // alert("API in Errore");
    return error;
  }
}
