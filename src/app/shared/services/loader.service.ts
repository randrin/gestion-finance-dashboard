import { Injectable, EventEmitter } from "@angular/core";

export const loaderKeys = {
  global: "global"
};

@Injectable({
  providedIn: "root"
})
export class LoaderService {
  loading = {
    [loaderKeys.global]: 0
  };

  onShowLoader: EventEmitter<any> = new EventEmitter();
  onHideLoader: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.getLoadingByKey = this.getLoadingByKey.bind(this);
    this.showLoader = this.showLoader.bind(this);
    this.hideLoader = this.hideLoader.bind(this);
  }

  getLoadingByKey(key) {
    return this.loading[key] > 0;
  }

  showLoader(key) {
    this.loading[key] = this.loading[key] ? ++this.loading[key] : 1;
    this.onShowLoader.emit(key);
  }

  hideLoader(key) {
    this.loading[key] = this.loading[key] ? --this.loading[key] : 0;
    this.onHideLoader.emit(key);
  }
}
