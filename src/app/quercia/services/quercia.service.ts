import { Injectable } from "@angular/core";
import { HttpUtilitiesService } from "src/app/shared/services/http-utilities.service";

const IN_LAVORAZIONE_TABS_URL = "assets/json/quercia/getInLavorazioneTabs.json";
const IN_LAVORAZIONE_DATA_URL = "assets/json/quercia/getInLavorazioneData.json";

const mapUlrsToApis = {};

@Injectable({
  providedIn: "root"
})
export class QuerciaService {
  constructor(private httpUtilities: HttpUtilitiesService) {}

  getInLavorazioneTabs() {
    return this.httpUtilities.customGet({ url: IN_LAVORAZIONE_TABS_URL }, true);
  }

  getInLavorazioneData(withLoader?, params = "") {
    return this.httpUtilities.customGet(
      { url: IN_LAVORAZIONE_DATA_URL + params },
      withLoader
    );
  }
}
