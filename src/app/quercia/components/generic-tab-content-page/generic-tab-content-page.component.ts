import { Component, OnInit } from "@angular/core";
import { GenericRoutePageComponent } from "src/app/shared/components/generic-route-page/generic-route-page.component";
import { QuerciaService } from "../../services/quercia.service";
import { genericDataChangeFn } from "src/app/shared/constants";

const paramsForObservable = {
  getAutoRefreshResult: response => (response ? response.tableData : {})
};

@Component({
  selector: "app-generic-tab-content-page",
  templateUrl: "./generic-tab-content-page.component.html",
  styleUrls: ["./generic-tab-content-page.component.scss"]
})
export class GenericTabContentPageComponent extends GenericRoutePageComponent
  implements OnInit {
  tableData: any = {};

  constructor(private querciaService: QuerciaService) {
    super(paramsForObservable);
    this.onManualRefreshClick = this.onManualRefreshClick.bind(this);
  }

  ngOnInit() {
    this.init();
  }

  fetchTableData(withLoader?, params?) {
    return this.querciaService.getInLavorazioneData(withLoader, params);
  }

  subscribeToFetchTableData(withLoader?, saveOldData?, params?) {
    return this.fetchTableData(withLoader, params).subscribe(onSuccess => {
      const { tableData } = onSuccess;
      this.tableData = tableData;
      if (saveOldData) {
        this.oldData = tableData;
      }
    });
  }

  init() {
    this.subscribeToFetchTableData(true, true);
    this.firstLoad = true;
    this.onManualRefresh.subscribe(newData => {
      this.tableData = newData;
    });
  }

  onAutoRefresh() {}
  dataChanged = genericDataChangeFn;
  getAutoRefreshObservable(params) {
    return this.fetchTableData(params);
  }

  onManualRefreshClick() {
    this.handleManualRefreshClick(true);
  }

  getHeadConfigFromTableData() {
    return (this.tableData && this.tableData.headConfigs) || {};
  }
  getBodyConfigFromTableData() {
    return (this.tableData && this.tableData.bodyConfigs) || {};
  }
  getInputSettingsFromTableData() {
    return (this.tableData && this.tableData.inputSettings) || {};
  }

  handleSubmit(event) {
    const elements = event.target.elements;

    const qs = elements.reduce
      ? elements.reduce((acc, current, index) => {
          if (current.name && (current.value || current.checked)) {
            let additionalUnpercent = "&";
            if (index === 0) {
              additionalUnpercent = "";
            }
            return `${acc}${additionalUnpercent}${
              current.name
            }=${current.value || current.checked}`;
          }
        }, "?")
      : "?";
   
      this.subscribeToFetchTableData(true, false, qs);
  }
}
