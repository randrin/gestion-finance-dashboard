import { Component, OnInit } from "@angular/core";

const getTabs = {
  tabs: [
    {
      id: "inLavorazione",
      label: "In Lavorazione",
      textChildren: "283",
      url: "/quercia/in-lavorazione"
    },
    {
      id: "inAttesa",
      label: "In Attesa",
      textChildren: "262",
      url: "/quercia/in-attesa"
    },
    {
      id: "Da Travasare",
      label: "Da Travasare",
      textChildren: "59",
      url: "/quercia/da-travasare"
    },
    {
      id: "Travasate",
      label: "Travasate",
      textChildren: "83",
      url: "/quercia/travasate"
    },
   /* {
      id: "annullate",
      label: "anullate",
      textChildren: "12",
      url: "/quercia/annullate"
    },
    {
      id: "reattivateDaSospensione",
      label: "reattivateDaSospensione",
      textChildren: "12",
      url: "/quercia/reattivateDaSospensione"
    }*/
  ]
};

@Component({
  selector: "app-quercia-page",
  templateUrl: "./quercia-page.component.html",
  styleUrls: ["./quercia-page.component.scss"]
})
export class QuerciaPageComponent implements OnInit {
  tabList = [];
  constructor() {}
  ngOnInit() {
    this.mockGetTabs();
    
  }
  mockGetTabs() {
    setTimeout(() => {
      this.tabList = getTabs.tabs;
    }, 1000);
  }
}

