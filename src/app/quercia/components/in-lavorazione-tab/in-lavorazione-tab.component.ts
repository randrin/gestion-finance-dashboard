import { Component, OnInit } from "@angular/core";
import { GenericLevel2PageComponent } from "../generic-level2-page/generic-level2-page.component";
import { Router } from "@angular/router";
import { QuerciaService } from "../../services/quercia.service";

@Component({
  selector: "app-in-lavorazione-tab",
  templateUrl: "./in-lavorazione-tab.component.html",
  styleUrls: ["./in-lavorazione-tab.component.scss"]
})
export class InLavorazioneTabComponent extends GenericLevel2PageComponent
  implements OnInit {
  tabs = [];
  constructor(
    private router: Router,
    private querciaService: QuerciaService
  ) {
    super();
  }
  ngOnInit() {
    this.init();
  }
  init() {
    this.querciaService.getInLavorazioneTabs().subscribe((success: any) => {
      const { tabs } = success;
      if (tabs) {
        this.tabs = tabs;
        this.handleAutoSelectFirstTab();
      }
    });
  }

  handleAutoSelectFirstTab() {
    if (this.tabs && this.tabs.length > 0) {
      const url = window.location.href;
      const matchIndex = this.tabs.findIndex(el => url.includes(el.url));
      if (matchIndex === -1) {
        const firstUrl = this.tabs[0].url;
        this.router.navigate([firstUrl]);
      }
    }
  }
}
