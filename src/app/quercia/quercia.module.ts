import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";

import { QuerciaPageComponent } from "./components/quercia-page/quercia-page.component";
import { GenericTabContentPageComponent } from "./components/generic-tab-content-page/generic-tab-content-page.component";
import { GenericLevel2PageComponent } from "./components/generic-level2-page/generic-level2-page.component";
import { InLavorazioneTabComponent } from "./components/in-lavorazione-tab/in-lavorazione-tab.component";
import { QuerciaRoutingModule } from "./quercia-routing-module";
import { SharedModule } from "../shared/shared.module";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    QuerciaPageComponent,
    GenericTabContentPageComponent,
    GenericLevel2PageComponent,
    InLavorazioneTabComponent
  ],
  imports: [HttpClientModule, QuerciaRoutingModule, SharedModule, CommonModule],
  exports: [QuerciaRoutingModule],
  entryComponents: [
    GenericTabContentPageComponent,
    GenericLevel2PageComponent,
    InLavorazioneTabComponent
  ],
  providers: []
})
export class QuerciaModule {}
