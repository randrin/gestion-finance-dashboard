import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { QuerciaPageComponent } from "./components/quercia-page/quercia-page.component";
import { GenericTabContentPageComponent } from "./components/generic-tab-content-page/generic-tab-content-page.component";
import { InLavorazioneTabComponent } from "./components/in-lavorazione-tab/in-lavorazione-tab.component";

const routes: Routes = [
  {
    path: "quercia",
    component: QuerciaPageComponent,
    children: [
      {
        path: "in-lavorazione",
        component: InLavorazioneTabComponent,
        children: [
          {
            path: "da-assegnare",
            component: GenericTabContentPageComponent
          },
          {
            path: "da-controllare",
            component: GenericTabContentPageComponent
          },
          {
            path: "sospese",
            component: GenericTabContentPageComponent
          },
          {
            path: "riattivate-da-sospensione",
            component: GenericTabContentPageComponent
          },
          {
            path: "bonifici-e-presentazioni-annullati",
            component: GenericTabContentPageComponent
          }
        ]
      },
      {
        path: "in-attesa",
        component: GenericTabContentPageComponent
      },
      {
        path: "da-travasare",
        component: GenericTabContentPageComponent
      },
      {
        path: "travasate",
        component: GenericTabContentPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class QuerciaRoutingModule {}
